package com.ruan.contentproviderexample;

import android.content.ContentResolver;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v7.app.ActionBarActivity;
import android.widget.ListView;
import android.widget.TextView;

import static android.provider.UserDictionary.Words;

public class Main extends ActionBarActivity {

    private static final String[] columnsFrom = new String[]{
            Words.WORD,
            Words.FREQUENCY
    };

    private static final int[] layoutTo = new int[]{
            android.R.id.text1,
            android.R.id.text2
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        ListView dictList = (ListView) findViewById(R.id.tv_dictionary);

        ContentResolver resolver = getContentResolver();

        Cursor cursor = resolver.query(Words.CONTENT_URI, null, null, null, null);

        SimpleCursorAdapter adapter = new SimpleCursorAdapter(this
                , android.R.layout.two_line_list_item
                , cursor
                , columnsFrom
                , layoutTo
                , 0
        );

        String header = "The user dictionary contains " + cursor.getCount() + " word(s)" +
                "\nWORD / FREQUENCY";

        TextView textView = new TextView(this);
        textView.setText(header);
        dictList.addHeaderView(textView);

        dictList.setAdapter(adapter);
    }
}
